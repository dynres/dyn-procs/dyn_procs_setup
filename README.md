# Time-X EuroHPC project: Setup for the Dynamic Resource Stack
## Introduction
This repository provides setup scripts for the Dynamic MPI prototype developed in the context of the EuroHPC Time-X project. It consists of the following sub-projects:
* [MPI Extensions](https://gitlab.inria.fr/dynres/dyn-procs/ompi)
* [PMIx Extensions](https://gitlab.inria.fr/dynres/dyn-procs/openpmix)
* [PRRTE Extensions](https://gitlab.inria.fr/dynres/dyn-procs/prrte)
* [Test Applications](https://gitlab.inria.fr/dynres/dyn-procs/test_applications)
* [A simple python-based Dynamic Resource Manager](https://gitlab.inria.fr/dynres/dyn-procs/dyn_rm)

## Setup environments

We provide setup scripts/instructions for the following envirnments:
* [Docker-Cluster](https://gitlab.inria.fr/dynres/dyn-procs/docker-cluster):
```
git clone -b docker_setup https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup.git
```
* Spack:
```
git clone -b spack_setup https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup.git
```
* Manual
```
git clone -b manual_setup https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup.git
```

## Contact
In case of any questions please contact me via email: `domi.huber@tum.de`


