#! /bin/bash

# Exit on errors
set -e

source env_vars_docker.sh
source config_docker.sh

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
mkdir -p $SCRIPT_DIR/output

function ERROR_MSG {
	LATEST_FILE=$(ls -Art1 $SCRIPT_DIR/output/output_*.txt | tail -n 1 2>/dev/null)

	if [ "$(echo "$LATEST_FILE" | wc -w)" != "0" ]; then
		echo "Showing last lines of $LATEST_FILE" 1>&2
		tail -n 20 "$LATEST_FILE" 1>&2
		echo "" 1>&2
		echo "Error, see above and also output_* files" 1>&2
	fi
	echo "Last command was:" 1>&2
	echo "$@" 1>&2

	exit 1
}

# Execute some program and its parameters, but first print out the program and parameters
function EXEC {
	echo "EXEC: $@" 1>&2

	# We also redirect stderr to stdout to catch it
	$@ 2>&1 || ERROR_MSG $@
}

if false; then
	echo "Exporting the hwloc and libevent install paths"
	export HWLOC_INSTALL_PATH=[/path/to/hwloc]
	export LIBEVENT_INSTALL_PATH=[/path/to/libevent]
	CONFIGURE_PARAMETERS=--with-hwloc=$HWLOC_INSTALL_PATH --with-libevent=$LIBEVENT_INSTALL_PATH
fi

if $CONFIG_BUILD_OPENPMIX; then

	echo
	echo "***********************************************"
	echo "* Building Open-PMIx..."
	echo "***********************************************"

	EXEC cd "$DYNMPI_BASE/"

	DST_DIR=./build/openpmix
	if [ -d "$DST_DIR" ]; then
		echo "Directory '$DST_DIR' already exists, skipping git clone"
	else
		EXEC mkdir -p build
		EXEC git clone $GIT_CLONE_PARAMS https://gitlab.inria.fr/dynres/dyn-procs/openpmix $DYNMPI_BASE/build/openpmix > $SCRIPT_DIR/output/output_openpmix_gitclone.txt || ERROR_MSG
	fi

	EXEC cd "$DYNMPI_BASE/build/openpmix"

	echo " + Running autogen.pl ..."
	EXEC ./autogen.pl > $SCRIPT_DIR/output/output_openpmix_autogen.txt

	echo " + Running configure ..."
	EXEC ./configure --prefix=$PMIX_ROOT $CONFIG_OPENPMIX_CONFIGURE > $SCRIPT_DIR/output/output_openpmix_configure.txt || ERROR_MSG

	echo " + Running make ..."
	EXEC make -j > $SCRIPT_DIR/output/output_openpmix_make.txt || ERROR_MSG

	echo " + Running make install ..."
	EXEC make all install > $SCRIPT_DIR/output/output_openpmix_make_install.txt || ERROR_MSG

	echo " + Building PMIx python module ..."
	EXEC cd "$DYNMPI_BASE/build/openpmix/bindings/python"
	EXEC python3 setup.py install --user > $SCRIPT_DIR/output/output_openpmix_python.txt || ERROR_MSG
	echo "Installation of Open-PMIx successful"
fi

#CONFIGURE_PARAMETERS="$CONFIGURE_PARAMETERS --includedir=$DYNMPI_BASE/install/pmix/include/"
CONFIGURE_PARAMETERS="$CONFIGURE_PARAMETERS"

if $CONFIG_BUILD_PRRTE; then

	echo
	echo "***********************************************"
	echo "* Building PRRTE..."
	echo "***********************************************"

	EXEC cd "$DYNMPI_BASE/"

	DST_DIR=./build/prrte
	if [ -d "$DST_DIR" ]; then
		echo "Directory '$DST_DIR' already exists, skipping git clone"
	else
		EXEC mkdir -p build
		EXEC git clone $GIT_CLONE_PARAMS https://gitlab.inria.fr/dynres/dyn-procs/prrte $DYNMPI_BASE/build/prrte > $SCRIPT_DIR/output/output_prrte_gitclone.txt || ERROR_MSG
	fi

	EXEC cd "$DYNMPI_BASE/build/prrte"

	echo " + Running autogen.pl ..."
	EXEC ./autogen.pl > $SCRIPT_DIR/output/output_prrte_autogen.txt || ERROR_MSG

	echo " + Running configure ..."
	echo ./configure --prefix=$PRRTE_ROOT $CONFIGURE_PARAMETERS -with-pmix=$PMIX_ROOT
	EXEC ./configure --prefix=$PRRTE_ROOT $CONFIGURE_PARAMETERS -with-pmix=$PMIX_ROOT $CONFIG_PRRTE_CONFIGURE > $SCRIPT_DIR/output/output_prrte_configure.txt || ERROR_MSG

	echo " + Running make ..."
	EXEC make -j > $SCRIPT_DIR/output/output_prrte_make.txt || ERROR_MSG

	echo " + Running make install ..."
	EXEC make all install > $SCRIPT_DIR/output/output_prrte_make_install.txt || ERROR_MSG
	echo "Installation of PRRTE successful"
fi

if $CONFIG_BUILD_OMP; then

	echo
	echo "***********************************************"
	echo "* Building Open-MPI..."
	echo "***********************************************"

	EXEC cd "$DYNMPI_BASE/"

	DST_DIR=./build/ompi
	if [ -d "$DST_DIR" ]; then
		echo "Directory '$DST_DIR' already exists, skipping git clone"
	else
		mkdir -p build
		EXEC git clone $GIT_CLONE_PARAMS https://gitlab.inria.fr/dynres/dyn-procs/ompi $DYNMPI_BASE/build/ompi > $SCRIPT_DIR/output/output_ompi_gitclone.txt || ERROR_MSG
	fi

	EXEC cd $DYNMPI_BASE/build/ompi

	echo " + Running autogen.pl ..."
	EXEC ./autogen.pl > $SCRIPT_DIR/output/output_ompi_autogen.txt || ERROR_MSG

	echo " + Running configure ..."
	EXEC ./configure --prefix=$OMPI_ROOT $CONFIGURE_PARAMETERS --with-pmix=$PMIX_ROOT --with-prrte=$PRRTE_ROOT --with-ucx=no $CONFIG_OMP_CONFIGURE > $SCRIPT_DIR/output/output_ompi_configure.txt || ERROR_MSG

	echo " + Running make ..."
	EXEC make -j > $SCRIPT_DIR/output/output_ompi_make.txt || ERROR_MSG

	echo " + Running make install ..."
        EXEC make all install > $SCRIPT_DIR/output/output_ompi_make_install.txt || ERROR_MSG
	echo "Installation of Open-MPI successful"
fi

if $CONFIG_WITH_TESTAPPS; then

	echo
	echo "***********************************************"
	echo "* Building Test applicatiions..."
	echo "***********************************************"
	
	EXEC cd "$DYNMPI_BASE/"
	
	DST_DIR=./build/test_applications
	if [ -d "$DST_DIR" ]; then
		echo "Directory '$DST_DIR' already exists, skipping git clone"
	else
		mkdir -p build
		EXEC git clone $GIT_CLONE_PARAMS https://gitlab.inria.fr/dynres/applications/miniapps.git $DYNMPI_BASE/build/test_applications > $SCRIPT_DIR/output/output_testapp_gitclone.txt || ERROR_MSG
	fi
	echo " + Running scons ..."
	EXEC cd "$DYNMPI_BASE/build/test_applications"
	EXEC scons example=DynMPISessions_v2a compileMode=release > $SCRIPT_DIR/output/output_test_app_v2a_scons.txt || ERROR_MSG
	EXEC scons example=DynMPISessions_v2a_nb compileMode=release > $SCRIPT_DIR/output/output_test_app_v2a_scons.txt || ERROR_MSG
	EXEC scons example=DynMPISessions_v2b compileMode=release > $SCRIPT_DIR/output/output_test_app_v2a_scons.txt || ERROR_MSG
	EXEC scons example=DynMPISessions_v2b_nb compileMode=release > $SCRIPT_DIR/output/output_test_app_v2a_scons.txt || ERROR_MSG
	EXEC scons example=DynMPISessions_v2a_replace compileMode=release > $SCRIPT_DIR/output/output_test_app_v2a_scons.txt || ERROR_MSG
	EXEC scons example=DynMPISessions_v2a_sleep compileMode=release > $SCRIPT_DIR/output/output_test_app_v2a_scons.txt || ERROR_MSG
	EXEC scons example=DynMPISessions_v2a_fortran compileMode=release > $SCRIPT_DIR/output/output_test_app_v2a_scons.txt || ERROR_MSG
	echo "Build of Test Applications successful"
fi

if $CONFIG_WITH_DYN_RM; then

	echo
	echo "***********************************************"
	echo "* Building the Dynamic Resource Manager ..."
	echo "***********************************************"
	
	EXEC cd "$DYNMPI_BASE/"
	
	DST_DIR=./build/dyn_rm
	if [ -d "$DST_DIR" ]; then
		echo "Directory '$DST_DIR' already exists, skipping git clone"
	else
		mkdir -p build
		EXEC git clone $GIT_CLONE_PARAMS https://gitlab.inria.fr/dynres/dyn-procs/dyn_rm $DYNMPI_BASE/build/dyn_rm > $SCRIPT_DIR/output/output_dyn_rm_gitclone.txt || ERROR_MSG
	fi
	echo "Build of Dynamic Resource Manager successful"
fi


if $CONFIG_WITH_LIBPFASST; then

	echo
	echo "***********************************************"
	echo "* Building LibPFASST ..."
	echo "***********************************************"

	EXEC cd "$DYNMPI_BASE/"

	DST_DIR=./build/LibPFASST
	if [ -d "$DST_DIR" ]; then
		echo "Directory '$DST_DIR' already exists, skipping git clone"
	else
		mkdir -p build
		EXEC git clone $GIT_CLONE_PARAMS https://gitlab.inria.fr/dynres/applications/libpfasst.git $DST_DIR > $SCRIPT_DIR/output/output_libpfasst_gitclone.txt || ERROR_MSG
	fi

	DST_DIR=./build/showcase_dyn_libpfasst
	if [ -d "$DST_DIR" ]; then
		echo "Directory '$DST_DIR' already exists, skipping git clone"
	else
		mkdir -p build
		EXEC git clone $GIT_CLONE_PARAMS https://github.com/boi4/showcase_dyn_libpfasst.git $DST_DIR > $SCRIPT_DIR/output/output_libpfasst_showcase_gitclone.txt || ERROR_MSG
	fi
	DST_DIR=./build/showcase_dyn_libpfasst/hypre
	if [ -d "$DST_DIR" ]; then
		echo "Directory '$DST_DIR' already exists, skipping git clone"
	else
		mkdir -p build
		EXEC git clone https://github.com/hypre-space/hypre.git $DST_DIR > $SCRIPT_DIR/output/output_libpfasst_showcase_hypre_gitclone.txt || ERROR_MSG
	fi

	cd  build/LibPFASST

	cp Makefile.examples/Makefile.FFT .

	echo -e "\nFFLAGS_EXTRA += -fallow-argument-mismatch" >> Makefile.local
	EXEC make $CONFIG_LIBPFASST_MAKE > $SCRIPT_DIR/output/output_libpfasst_make.txt || ERROR_MSG

	# build libpfasst showcase
	cd ../showcase_dyn_libpfasst

	# build dependency
	cd hypre/src
	EXEC ./configure $CONFIG_HYPRE_CONFIGURE > $SCRIPT_DIR/output/output_hypre_configure.txt || ERROR_MSG
	EXEC make -j > $SCRIPT_DIR/output/output_hypre_make.txt || ERROR_MSG
	cd ../..

	EXEC make > $SCRIPT_DIR/output/output_libpfasst_showcase_make.txt || ERROR_MSG

	echo "Build of LibPFASST successful"
fi
